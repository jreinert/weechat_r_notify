require 'drb'

SCRIPT_NAME = 'r_notify'
AUTHOR = 'Joakim Reinert'
VERSION = '0.1'
LICENSE = 'GPL3'
DESCRIPTION =<<END
A notification server that clients can connect to and listen for notifications.
Inspired by remote-notify by Bluewind.
END
SHUTDOWN_FUNCTION = 'kill_server'
ENCODING = '' # Defaults to UTF-8
SERVER_EXECUTABLE = File.join(ENV['HOME'], 'notification_server.rb')


@default_options = {
  "show_hilights" => "on",
  "show_priv_msg" => "on",
  "smart_notification" => "off",
  "password" => "change me",
  "notification_server_port" => "9000",
}

def weechat_print(message)
  Weechat.print("", "#{SCRIPT_NAME}: #{message}")
end
  
def notify_show(data, bufferp, uber_empty, tagsn, isdisplayed, ishilight, prefix, message)
  unless (Weechat.config_get_plugin('smart_notification') == "on") &&
    (bufferp == Weechat.current_buffer)
  
    if (Weechat.buffer_get_string(bufferp, "localvar_type") == "private") &&
      (Weechat.config_get_plugin('show_priv_msg') == "on")
  
        @notification_server.push(@password, {prefix: prefix, message: message})
    elsif (ishilight == "1") && (Weechat.config_get_plugin('show_hilights') == "on")
      buffer = Weechat.buffer_get_string(bufferp, "short_name") || Weechat.buffer_get_string(bufferp, "name")
      @notification_server.push(@password, {buffer: buffer, prefix: prefix, message: message})
      weechat_print('Got highlight')
    end
  end
  Weechat::WEECHAT_RC_OK
end
  
def kill_server(*args)
  system('rnotify-server -a stop')
  Weechat::WEECHAT_RC_OK
end
  
def weechat_init
  Weechat.register(SCRIPT_NAME, AUTHOR, VERSION, LICENSE, DESCRIPTION.chomp, SHUTDOWN_FUNCTION, ENCODING)
  @default_options.each do |key, value|
    if Weechat.config_get_plugin(key).empty?
      Weechat.config_set_plugin(key, value)
    end
  end
  port = Weechat.config_get_plugin('notification_server_port')
  server_uri = "druby://:#{port}"
  @password = Weechat.config_get_plugin('password')
  pipe = IO.popen("rnotify-server -p #{port} -P #{@password} -d")
  weechat_print pipe.gets
  @notification_server = DRbObject.new_with_uri(server_uri)
  Weechat.hook_print("", "notify_message", "", 1, "notify_show", "")
  Weechat.hook_print("", "notify_private", "", 1, "notify_show", "")
  Weechat::WEECHAT_RC_OK
end
